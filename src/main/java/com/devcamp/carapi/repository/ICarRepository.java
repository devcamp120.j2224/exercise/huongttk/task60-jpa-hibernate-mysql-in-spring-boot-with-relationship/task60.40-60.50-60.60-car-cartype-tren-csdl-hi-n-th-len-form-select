package com.devcamp.carapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.carapi.model.CCar;

public interface ICarRepository extends JpaRepository <CCar, Integer>{
    CCar findByCarCode(String car);
}
