package com.devcamp.carapi.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.carapi.model.CCar;
import com.devcamp.carapi.model.CCarType;
import com.devcamp.carapi.repository.ICarRepository;


@RestController
@CrossOrigin
@RequestMapping
public class CarTypecontroller {
    @Autowired
    ICarRepository carRepository;
    @GetMapping("devcamp-cartypes")
    public ResponseEntity <Set<CCarType>> getCarTypeByCarCode(@RequestParam(value = "carCode")String carCode){
        try {
            CCar vCar = carRepository.findByCarCode(carCode);
            if (vCar != null) {
                return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
