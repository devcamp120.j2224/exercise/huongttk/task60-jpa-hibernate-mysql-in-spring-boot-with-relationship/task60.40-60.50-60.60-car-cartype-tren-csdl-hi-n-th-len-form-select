package com.devcamp.carapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.carapi.model.CCarType;

public interface ITypeCarRepository extends JpaRepository <CCarType, Integer>{
    
}
