package com.devcamp.carapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.carapi.model.CCar;
import com.devcamp.carapi.repository.ICarRepository;

@RestController
@CrossOrigin
@RequestMapping
public class CarController {
    @Autowired
    ICarRepository carRepository;
    @GetMapping("devcamp-cars")
    public ResponseEntity <List<CCar>>getAllCar(){
        try {
            List<CCar> listCar = new ArrayList<CCar>();
            carRepository.findAll().forEach(listCar :: add);
            return new ResponseEntity<>(listCar, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
